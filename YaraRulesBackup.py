import json
import requests
import configparser

config = configparser.ConfigParser()
config.read('config.ini')
TOKEN = config['DEFAULT']['TOKEN']

header = {"Authorization": "Token %s" % TOKEN}
path = './rules/'
url = 'https://api.koodous.com/user_rulesets?page=1&page_size=50&active=True&ordering=-modified_on'

resp = requests.get(url=url, headers=header)

if resp.status_code == 200:
    doc = resp.json()
    for result in doc['results']:
        f1 = open(path + result['name'].replace(' ','_') + "__" + str(result['id']) + ".yar", 'w')
        f1.write(''.join(result['rules']))
        f1.close()