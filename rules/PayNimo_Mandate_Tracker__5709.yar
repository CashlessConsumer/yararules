import "androguard"

rule PayNimoMandateActivity
{
	meta:
		description = "All PayNimo Mandate Activity Tracker"
	condition:
		androguard.activity("com.paynimo.android.payment.DigitalMandateActivity")
}