import "androguard"

rule QuaggaSDKTrackerActivity
{
	meta:
		description = "All Quagga SDK Apps"
	condition:
		androguard.activity("quagga.com.sdk.ConsentActivity") or
		androguard.activity("com.aadhaarapi.sdk.gateway_lib.qtActivity.AadhaarAPIActivity")
}