import "androguard"

rule AePSMicroATM
{
	meta:
		description = "Detect All AePS apps built for MicroATM agents by a platform X"
	condition:
		androguard.url("aepsandroidapp.firebaseio.com")
}