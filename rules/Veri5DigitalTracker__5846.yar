import "androguard"

rule Veri5DigitalTracker
{
	meta:
		description = "This rule detects Veri5 Digital SDK"
	strings:
		$a = "https://sandbox.veri5digital.com/video-id-kyc/api/1.0/"
		$b = "https://prod.veri5digital.com/video-id-kyc/api/1.0/"
	condition:
		($a or $b)  and
		androguard.permission(/android.permission.INTERNET/)
}