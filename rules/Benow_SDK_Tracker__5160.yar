import "androguard"

rule BenowSDKTrackerActivity
{
	meta:
		description = "All Benow SDK Apps"
	condition:
		androguard.activity("com.benow.paymentsdk.activities.WebViewActivity")
}