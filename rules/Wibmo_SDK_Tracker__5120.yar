import "androguard"

rule WibmoSDKTrackerActivity
{
	meta:
		description = "All Wibmo SDK Apps"
	condition:
		androguard.activity("com.enstage.wibmo.sdk.inapp.InAppInitActivity")
}