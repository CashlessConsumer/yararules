import "androguard"

rule RuPayTracker
{
	meta:
		description = "This rule detects RuPay merchant verification"
	strings:
		$a = "https://swasrec.npci.org.in"
		$b = "https://swasrec2.npci.org.in"
		$c = "https://mwsrec.npci.org.in/MWS/Scripts/MerchantScript_v1.0.js"
	condition:
		($a or $b or $c) and
		androguard.permission(/android.permission.INTERNET/)
}