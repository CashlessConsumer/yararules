import "androguard"

rule ZoopOneSDKTracker
{
	meta:
		description = "All Zoop One SDK Apps"
	condition:
		androguard.activity("sdk.zoop.one.offline_aadhaar.zoopActivity.ZoopConsentActivity") or
		androguard.activity("one.zoop.sdkesign.esignlib.qtActivity.QTApiActivity")
}