import "androguard"

rule netc_detect
{
	meta:
		description = "This rule detects FASTag apps"
	strings:
		$a = "http://npci.org/etc/schema"
	condition:
		($a) and
		androguard.permission(/android.permission.INTERNET/)		
}