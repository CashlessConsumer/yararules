import "androguard"

rule AirPaySDKActivity
{
	meta:
		description = "All AirPay SDK Apps"
	condition:
		androguard.activity("com.airpay.airpaysdk_simplifiedotp.AirpayActivity")
}