import "androguard"

rule AxisMerchantSDKActivity
{
	meta:
		description = "All Axis Merchant SDK Apps"
	condition:
		androguard.activity("com.axis.axismerchantsdk.activity.PayActivity")
}