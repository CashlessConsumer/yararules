import "androguard"

rule CashFreeSDKTracker
{
	meta:
		description = "All CashFree SDK Apps"
	condition:
		( androguard.activity("com.gocashfree.cashfreesdk.CFPaymentActivity") or
		androguard.activity("com.gocashfree.cashfreesdk.CFUPIPaymentActivity") or
		androguard.activity("com.gocashfree.cashfreesdk.AmazonPayActivity") or
		androguard.activity("com.gocashfree.cashfreesdk.GooglePayActivity") or
		androguard.activity("com.gocashfree.cashfreesdk.CFPhonePayActivity"))
}