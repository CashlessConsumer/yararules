import "androguard"

rule bbps_detect
{
	meta:
		description = "This rule detects BBPS apps"

	strings:
		$a = "http://bbps.org/schema"
		$b = "bbps/BillFetchRequest/1.0/"
		$c = "bbps/BillPaymentRequest/1.0"
	condition:
		($a or $b or $c) and
		androguard.permission(/android.permission.INTERNET/)		
}