import "androguard"

rule EBSPaymentsSDKActivity
{
	meta:
		description = "All EBS Payments SDK Apps"
	condition:
		androguard.activity("com.ebs.android.sdk.PaymentDetailActivity")		
}