import "androguard"

rule JDPaySDKTrackerActivity
{
	meta:
		description = "All JDPay SDK Apps"
	condition:
		androguard.activity("com.justdialpayui.PaymentsActivity")
}