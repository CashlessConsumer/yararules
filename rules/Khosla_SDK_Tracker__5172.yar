import "androguard"
  
rule KhoslaSDKTrackerActivity
{
        meta:
                description = "All Khosla SDK Apps"
        condition:
                androguard.activity("com.khoslalabs.aadhaarbridge.AadhaarBridge")
}