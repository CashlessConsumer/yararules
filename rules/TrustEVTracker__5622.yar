import "androguard"

rule TrustEVTracker
{
	meta:
		description = "This rule detects TransUnion TrustEV SDK"
	strings:
		$a = "https://app.trustev.com/api/v2.0/session"
	condition:
		$a  and
		androguard.permission(/android.permission.INTERNET/)
}