import "androguard"

rule LeegalitySDKTracker
{
	meta:
		description = "All Leegality SDK Apps"
	condition:
		androguard.activity("com.leegality.leegality.Leegality")
}