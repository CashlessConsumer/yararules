import "androguard"

rule LotusPaySDKTrackerActivity
{
	meta:
		description = "All LotusPay SDK Apps"
	condition:
		androguard.activity("com.lotuspay.library.LotusPay")	
}