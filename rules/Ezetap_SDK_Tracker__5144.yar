import "androguard"

rule EzetapSDKTracker
{
	meta:
		description = "All Ezetap SDK Apps"
	condition:
		androguard.activity("com.eze.api.EzeAPIActivity")
}