import "androguard"

rule GSTPaymentTracker
{
	meta:
		description = "This rule detects All apps with GST Payment link"
	strings:
		$a = "https://payment.gst.gov.in/payment/"
		$b = "https://payment.gst.gov.in/payment/trackpayment"
	condition:
		($a or $b) and
		androguard.permission(/android.permission.INTERNET/)
}