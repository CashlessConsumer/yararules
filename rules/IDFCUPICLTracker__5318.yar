import "androguard"

rule IDFCUPICLTracker
{
	meta:
		description = "All IDFC UPI SDK Apps"
	condition:
		androguard.activity("com.fss.idfc.idfcupicl")
}