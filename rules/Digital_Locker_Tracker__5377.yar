import "androguard"

rule DigitalLockerTracker
{
	meta:
		description = "This rule detects DigitalLocker SDK"
	strings:
		$a = "https://api.digitallocker.gov.in/"
		$b = "https://api.digitallocker.gov.in/public/oauth2/1/token"
		$c = "https://api.digitallocker.gov.in/public/oauth2/1/authorize"		
	condition:
		($a or $b or $c) and
		androguard.permission(/android.permission.INTERNET/)
}