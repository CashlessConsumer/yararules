import "androguard"

rule AmazonPayINSDKActivity
{
	meta:
		description = "All Amazon Pay India Apps"
	condition:
		androguard.activity("amazonpay.silentpay.APayActivity")
}