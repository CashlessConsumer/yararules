import "androguard"

rule ZaakPayTracker
{
	meta:
		description = "This rule detects ZaakPay gateway powered apps"
	strings:
		$a = "https://api.zaakpay.com/zaakpay.js"
		$b = "https://api.zaakpay.com/"
	condition:
		($a or $b) and
		androguard.permission(/android.permission.INTERNET/)		
}