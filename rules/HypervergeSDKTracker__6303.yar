import "androguard"

rule HypervergeSDKTracker
{
	meta:
		description = "All Hyperverge SDK Apps"
	condition:
		androguard.activity("co.hyperverge.hypersnapsdk.activities.HVFaceActivity") or
		androguard.activity("co.hyperverge.hvinstructionmodule.activities.FaceInstructionActivity")
}