import "androguard"

rule algo360_detect
{
	meta:
		description = "This rule detects Algo360 Credit Score SDK apps"

	strings:
		$a = "iapi.algo360.com"
		$b = "https://uat.algo360.com:7777"		
	condition:
		($a or $b) and
		androguard.permission(/android.permission.INTERNET/)		
}