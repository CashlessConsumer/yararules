import "androguard"

rule FingPayActivity
{
	meta:
		description = "All FingPay SDK Apps"
	strings:
		$a = "https://fingpayap.tapits.in/fpaepsservice/"
	condition:
		($a) or
		androguard.activity("com.tapits.fingpay.FingerPrintScreen")
		

}