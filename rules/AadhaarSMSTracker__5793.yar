import "androguard"
import "droidbox"

rule AadhaarSMSTracker
{
	meta:
		description = "This rule detects apps using Aadhaar SMS"
	strings:
		$a = "GVID"
		$b = "RVID"
		$c = "GETOTP"		
	condition:
		($a or $b or $c) and
		droidbox.sendsms("1947") and
		androguard.permission(/android.permission.SEND_SMS/)
}