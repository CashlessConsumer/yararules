import "androguard"

rule WinjitMusicTracker
{
	meta:
		description = "All Winjit SDK Apps"
	condition:
		androguard.activity("com.winjit.musiclib.AboutUsAct")
}