import "androguard"

rule CitrusSDKActivity
{
	meta:
		description = "All Citrus SDK Apps"
	condition:
		androguard.activity("com.citrus.sdk.CitrusActivity")
}