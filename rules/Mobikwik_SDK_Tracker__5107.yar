import "androguard"

rule MobikwikSDKActivity
{
	meta:
		description = "All Mobikwik SDK Apps"
	condition:
		androguard.activity("com.mobikwik.sdk.PaymentActivity")
}