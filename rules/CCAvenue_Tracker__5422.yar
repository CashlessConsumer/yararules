import "androguard"

rule CCAvenueTracker
{
	meta:
		description = "All CCAvenue SDK Apps"
	condition:
		androguard.activity("com.ccavenue.indiasdk.PayOptionsActivity")		
}