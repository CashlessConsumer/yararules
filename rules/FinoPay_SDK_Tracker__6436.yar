import "androguard"

rule FinoPaySDKTrackerActivity
{
	meta:
		description = "All Fino SDK Apps"
	condition:
		androguard.activity("com.finopaytech.finosdk.activity.DeviceSettingActivity") or
		androguard.activity("com.finopaytech.finosdk.fragments.BTDiscoveryFragment") or
		androguard.activity("com.finopaytech.finosdk.activity.MainTransactionActivity") or
		androguard.activity("com.finopaytech.finosdk.activity.TransactionStatusActivity")
}