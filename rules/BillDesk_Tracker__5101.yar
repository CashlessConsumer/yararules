import "androguard"

rule BillDeskPayActivity
{
	meta:
		description = "All BillDesk SDK Apps"
	condition:
		androguard.activity("com.billdesk.sdk.QuickPayView")		
}