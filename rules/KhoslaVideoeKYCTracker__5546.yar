import "androguard"

rule KhoslaVideoeKYCTracker
{
	meta:
		description = "All Khosla Video eKYC SDK Apps"	
	condition:		
		androguard.activity("com.khoslalabs.videoidkyc.ui.init.VideoIdKycInitActivity")
}