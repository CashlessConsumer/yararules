import "androguard"

rule PhonePeActivity
{
	meta:
		description = "All Phonepe SDK Apps"
	condition:
		androguard.activity("com.phonepe.android.sdk.ui.MerchantTransactionActivity") or
		androguard.activity("com.phonepe.android.sdk.ui.debit.views.TransactionActivity")		
}

rule YesBankActivity
{
	meta:
		description = "All YesBank UPI SDK"
	condition:
		androguard.activity("com.yesbank.TransactionStatus")
}