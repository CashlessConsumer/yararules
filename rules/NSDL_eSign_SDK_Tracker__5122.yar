import "androguard"

rule NSDLESignSDKTrackerActivity
{
	meta:
		description = "All NSDL eSign SDK Apps"
	condition:
		androguard.activity("com.nsdl.egov.esignaar.NsdlEsignActivity")
}