import "androguard"

rule iServeUSDKActivity
{
	meta:
		description = "All iServeU AePS SDK Apps"
	condition:
		androguard.activity("com.iserveu.aeps.aepslibrary.dashboard.DashboardActivity") or
		androguard.activity("com.iserveu.aeps.aepslibrary.microatm.MicroAtmActivity") or
		androguard.activity("com.iserveu.aeps.aepslibrary.WelcomeMATMSdkActivity") or
		androguard.activity("com.iserveu.aeps.aepslibrary.transaction.ReportActivity") or
		androguard.activity("com.iserveu.aeps.aepslibrary.transactionstatus.TransactionStatusActivity") or
		androguard.activity("com.iserveu.aeps.aepslibrary.transaction.TransactionReceiptActivity")
}