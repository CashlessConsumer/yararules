import "androguard"

rule AtomSDKTracker
{
	meta:
		description = "All Atom SDK Apps"
	condition:
		androguard.activity("com.atom.mobilepaymentsdk.PayActivity")		
}