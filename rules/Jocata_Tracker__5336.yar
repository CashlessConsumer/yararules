import "androguard"

rule JocataTracker
{
	meta:
		description = "This rule detects Jocata API"
	strings:
		$a = "https://evoke.jocatagrid.in/EvokeClient"
		$b = "https://evoke.jocatagrid.in"
		$c = "jocatagrid.in"
	condition:
		($a or $b or $c) and
		androguard.permission(/android.permission.INTERNET/)
}