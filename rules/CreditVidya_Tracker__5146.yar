import "androguard"

rule CreditVidyaTracker
{
	meta:
		description = "This rule detects CreditVidya SDK"
	strings:
		$a = "https://api.creditvidya.com"
		$b = "https://api.creditvidya.com/sdk/api/"
		$c = "https://api.creditvidya.com/sdk/api/token/v3"		
	condition:
		($a or $b or $c) and
		androguard.permission(/android.permission.INTERNET/)
}