import "androguard"

rule aadhaar_vid_generators
{
	meta:
		description = "This rule detects Aadhaar VID Generation in apps"
	strings:
		$a = "https://resident.uidai.gov.in/web/resident/vidgeneration"
		$b = "https://resident.uidai.gov.in/vidgeneration"		
	condition:
		($a or $b) and
		androguard.permission(/android.permission.INTERNET/)		
}