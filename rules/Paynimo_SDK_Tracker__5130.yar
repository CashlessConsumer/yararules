import "androguard"

rule PayNimoActivity
{
	meta:
		description = "All PayNimo SDK Apps"
	condition:
		androguard.activity("com.paynimo.android.payment.PaymentActivity")
}