import "androguard"

rule PayUActivity
{
	meta:
		description = "All PayU SDK Apps"
	condition:
		androguard.activity("com.payu.payuui.Activity.PayUBaseActivity")
}