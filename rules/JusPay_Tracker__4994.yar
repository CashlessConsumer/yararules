import "androguard"

rule JusPayActivity
{
	meta:
		description = "All JusPay SDK Apps"
	condition:
		androguard.activity("in.juspay.godel.PaymentActivity")	or
		androguard.activity("in.juspay.juspaysafe.LegacyPaymentActivity")

}