import "androguard"

rule DigioESignSDKTrackerActivity
{
	meta:
		description = "All Digio eSign SDK Apps"
	strings:
		$a = "https://ext.digio.in"
	condition:
		($a or
		androguard.activity("com.digio.in.esign2sdk.DigioEsignActivity"))
}