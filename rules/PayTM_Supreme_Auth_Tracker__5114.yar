import "androguard"

rule PayTMSupremeAuthActivity
{
	meta:
		description = "All PayTM auth Apps"
	condition:
		androguard.activity("com.one97.supreme.ui.auth.SupremeAuthActivity")
}