import "androguard"

rule FreechargeINSDKActivity
{
	meta:
		description = "All Freecharge India Apps"
	condition:
		androguard.activity("in.freecharge.checkout.android.pay.PayInitActivity")
}