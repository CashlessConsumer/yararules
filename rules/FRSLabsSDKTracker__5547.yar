import "androguard"

rule FRSLabsSDKTracker
{
	meta:
		description = "All FRSLabs SDK Apps"
	condition:
		androguard.activity("com.frslabs.android.sdk.scanid.activities.IDScannerActivity") or
		androguard.activity("com.frslabs.android.sdk.facesdk.activities.FaceCaptureActivity") or
		androguard.activity("com.frslabs.android.sdk.videosdk.ui.WorkflowActivity")
}