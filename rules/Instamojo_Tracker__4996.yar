import "androguard"

rule InstamojoActivity
{
	meta:
		description = "All Instamojo SDK Apps"
	condition:
		androguard.activity("com.instamojo.android.activities.PaymentActivity")
}