import "androguard"

rule FinBoxINTracker
{
	meta:
		description = "This rule detects FinBox India SDK"
	strings:
		$a = "https://riskmanager.apis.finbox.in"
		$b = "https://api.finbox.in/api"
		$c = "https://logger.apis.finbox.in"		
	condition:
		($a or $b or $c) and
		androguard.permission(/android.permission.INTERNET/)
}