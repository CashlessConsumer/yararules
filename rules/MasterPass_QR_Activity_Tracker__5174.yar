import "androguard"

rule MasterPassQRActivityTracker
{
	meta:
		description = "All Masterpass QR Scan Apps"
	condition:
		androguard.activity("com.masterpassqrscan.MasterPassQrCodeCaptureActivity")
}