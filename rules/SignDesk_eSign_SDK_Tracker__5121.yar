import "androguard"

rule SignDeskESignSDKTrackerActivity
{
	meta:
		description = "All SignDesk eSign SDK Apps"
	condition:
		androguard.activity("in.signdesk.esignsdk.esign.eSign")
}