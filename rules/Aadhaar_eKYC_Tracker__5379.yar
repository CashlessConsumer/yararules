import "androguard"

rule AadhaareKYCTracker
{
	meta:
		description = "This rule detects potential Aadhaar eKYC apps"
	strings:
		$a = "Aadhaar"
		$b = "eKYC"
		$c = "eSign"		
	condition:
		(($a) and ($b or $c)) and
		androguard.permission(/android.permission.INTERNET/)
}