import "androguard"

rule zipnach_detect
{
	meta:
		description = "This rule detects ZIPNach powered apps"
	strings:
		$a = "http://uat1.zipnach.com"
	condition:
		$a and
		androguard.permission(/android.permission.INTERNET/)		
}